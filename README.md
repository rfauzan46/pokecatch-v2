# PokeCatch v2

PokeCatch v2 is a web application for Pokemon enthusiasts to explore and collect information about Pokemon characters, abilities, moves, and more. Users can also catch their favorite Pokemon and add them to a "Caught" list.

# Website Screenshots
![Pokedex](/ss/pokedex.png)
![Pokemon Details](/ss/details.png)
![Caught Pokemons](/ss/caught.png)



## Features

- Browse and search Pokemon characters
- View detailed information about each Pokemon
- Catch a Pokemon
- View a list of caught Pokemon

## Technologies Used

- React.js: Front-end JavaScript library for building user interfaces
- Redux: State management library for managing application state
- Material-UI: UI component library for styling the application
- Axios: HTTP client for making API requests
- React Router: Library for handling routing in the application
- Node.js: JavaScript runtime environment for server-side development
- Express: Web application framework for Node.js
- MongoDB: NoSQL database for storing user data

## Installation

1. Clone the repository:

   ```shell
   git clone https://gitlab.com/rfauzan46/pokecatch-v2.git
   ```

2. Navigate to the project directory:

   ```shell
   cd pokecatch-v2
   ```

3. Install the dependencies:

   ```shell
   npm install --legacy-peer-deps
   ```

4. Start the development server:

   ```shell
   npm start
   ```

5. Open the application in your browser:

   ```shell
   http://localhost:3000
   ```

## API Reference

PokeCatch v2 utilizes the following APIs:

- [PokeAPI](https://pokeapi.co/): RESTful API for retrieving Pokemon data

## Acknowledgements

- [PokeAPI](https://pokeapi.co/) for providing the Pokemon data
- [Material-UI](https://material-ui.com/) for the UI components
- [React Router](https://reactrouter.com/) for handling routing in React applications
- [Axios](https://axios-http.com/) for making HTTP requests