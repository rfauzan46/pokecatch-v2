import { INITIALIZE_CAUGHT_LIST,ADD_CAUGHT_POKEMON, ADD_CAUGHT_POKEMON_FAILURE, REMOVE_CAUGHT_POKEMON } from '../actions/types';

const initialState = {
  caughtList: [],
};

const caughtReducers = (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZE_CAUGHT_LIST:
      return {
        ...state,
        caughtList: action.payload,
      };
    case ADD_CAUGHT_POKEMON:
    return {
        ...state,
        caughtList: [...state.caughtList, action.payload],
    };

    case ADD_CAUGHT_POKEMON_FAILURE:
    console.log('Failed to catch the Pokemon!');
    return state;

    case REMOVE_CAUGHT_POKEMON:
      const updatedList = state.caughtList.filter((pokemon) => 
      pokemon.id !== action.payload.id)
      return {
        ...state,
        caughtList: updatedList,
      }
    default:
      return state;
  }
};

export default caughtReducers;
