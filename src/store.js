import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import caughtReducers from './reducers/caughtReducers';

const store = createStore(caughtReducers, applyMiddleware(thunk));

export default store;
