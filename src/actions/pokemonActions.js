import { INITIALIZE_CAUGHT_LIST, ADD_CAUGHT_POKEMON, ADD_CAUGHT_POKEMON_FAILURE, REMOVE_CAUGHT_POKEMON } from './types';

export const initializeCaughtList = (caughtList) => {
  return {
    type: INITIALIZE_CAUGHT_LIST,
    payload: caughtList,
  };
};

export const addCaughtPokemon = (pokemon) => {
  return (dispatch, getState) => {
    const isSuccess = Math.random() < 0.5;
    const state = getState();
    const caughtList = state.caughtList;

    if (isSuccess) {
      alert(`Congratulations! You caught ${pokemon.name}!`);
      const updatedCaughtList = [...caughtList, pokemon];
      localStorage.setItem('caughtList', JSON.stringify(updatedCaughtList));
      dispatch({
        type: ADD_CAUGHT_POKEMON,
        payload: pokemon,
      });
    } else {
      alert(`Oops! Failed to catch ${pokemon.name}. Try again!`);
      dispatch({
        type: ADD_CAUGHT_POKEMON_FAILURE,
      });
    }
  };
};

export const removeCaughtPokemon = (pokemon) => {
  return (dispatch, getState) => {
    const state = getState();
    const caughtList = state.caughtList;
    alert(`${pokemon.name} was removed from your pocket. Catch other pokemons!`);
    const updatedCaughtList = caughtList.filter((p) => p.id !== pokemon.id);
    localStorage.setItem('caughtList', JSON.stringify(updatedCaughtList));

    dispatch({
      type: REMOVE_CAUGHT_POKEMON,
      payload: pokemon,
    });
  };
};
