import { AppBar, Toolbar, Typography } from '@material-ui/core'
import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    AppBar: {
        backgroundColor: '#948d6b'
    },
    link: {
        textDecoration: 'none'
    },
    title: {
        cursor: 'pointer',
        color: 'white',
        marginRight:50,
    }

}))

export default function AppNavigator() {
    const classes = useStyles()
  return (
    <div>
        <AppBar className={classes.AppBar} position="fixed">
            <Toolbar>
                <Link to="/" className={classes.link}>
                    <Typography className={classes.title}>Pokedex</Typography>
                </Link>
                <Link to="/caught" className={classes.link}>
                    <Typography className={classes.title}>Caught</Typography>
                </Link>
            </Toolbar>
        </AppBar>
    </div>
  )
}
