import { Card, CardContent, CardMedia, Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'

const useStyles = makeStyles(() => ({
    card: {
        cursor: 'pointer',
        backgroundColor: '#948d6b',
        textTransform: 'capitalize',
        "&:hover": {
            backgroundColor: '#514d3b',
        }
    },
    cardImage: {
        margin: "auto",
        width: 100,
        height:100
    },
    cardContent: {
        textAlign: "center",
    },
    link: {
        textDecoration: "none",
        color:'white'
    }
}))

export default function PokemonCard(props) {
    const classes = useStyles()
    const { pokemon, image } = props;
    const { id, name } = pokemon;
    return (
    <Grid item xs={12} sm={2} key={id}>
        <Card className={classes.card}>
            <Link className={classes.link} to={"/pokemon/" + id}>
                <CardMedia className={classes.cardImage} image={image}></CardMedia>
                <CardContent className={classes.cardContent}>
                    <Typography>{name}</Typography>
                </CardContent>
            </Link>
        </Card>
    </Grid>
  )
}
