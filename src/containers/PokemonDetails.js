import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { POKEMON_API_URL } from '../config';
import { useParams } from 'react-router-dom';
import { CircularProgress, Box, makeStyles, Grid } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { addCaughtPokemon, removeCaughtPokemon } from '../actions/pokemonActions';


const useStyles = makeStyles(() => ({
  pokedexContainer: {
    padding: '100px 0px 0px 0px',
    height: '200vh',
    backgroundColor: '#fffecc',
    textTransform: 'capitalize',
    textAlign: 'center',
    borderRadius: 5,
  },
  pokemonName: {
    fontSize: 50,
    margin: '-10px 0',
    textAlign: 'center',
    fontFamily: 'Roboto'
  },
  pokemonImage: {
    width: '250px',
    height: '250px',
    margin: '0 auto',
    display: 'block',
  },
  pokemonInfo: {
    fontSize: '24px',
    fontFamily: 'Roboto',
  }
}));

const PokemonDetails = () => {
  const classes = useStyles();
  const { id } = useParams();
  const [pokemon, setPokemon] = useState(null);
  const dispatch = useDispatch();
  const caughtList = useSelector((state) => state.caughtList);
  const isPokemonCaught = pokemon && caughtList.some((caughtPokemon) => caughtPokemon.id === pokemon.id);
  const handleAction = () => {
    if (isPokemonCaught) {
      dispatch(removeCaughtPokemon(pokemon));
    } else {
      dispatch(addCaughtPokemon(pokemon));
    }
  };

  useEffect(() => {
    axios
      .get(POKEMON_API_URL + '/' + id)
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          console.log(response.data);
          setPokemon(response.data);
        }
      })
      .catch((error) => {
        console.error('Error fetching Pokemon details:', error);
      });
  }, [id]);

  return (
    <Box>
      {pokemon ? (
        <Grid className={classes.pokedexContainer} container spacing={2}>
          <Grid item xs={12}>
            <h1 className={classes.pokemonName}>{pokemon.name}</h1>
          </Grid>
          <Grid item xs={12}>
            <img
              className={classes.pokemonImage}
              src={pokemon.sprites.other.dream_world.front_default}
              alt={pokemon.name}
            />
          </Grid>
          <Grid item xs={12}>
            {isPokemonCaught ? 
            (<button onClick={handleAction}>Remove This Pokemon :( </button>) 
            : (<button onClick={handleAction}>Catch This Pokemon!</button>)
            }
          </Grid>
          <Grid item xs={12} className={classes.pokemonInfo}>
            <Grid container justifyContent="center">
                <Grid item md={1}>
                    <span style={{fontWeight: 'bold'}}>Name</span>
                    <br/>
                    {pokemon.name}
                </Grid>
                <Grid item md={1}>
                    <span style={{fontWeight: 'bold'}}>Type</span>
                    <br/>
                    {pokemon.types.map((type, index) => (
                    <Grid container justifyContent="center">
                         <Grid item md={2} key={index}>
                        {type.type.name}
                        </Grid>
                    </Grid>
                    ))}
                </Grid>
                <Grid item md={1}>
                    <span style={{fontWeight: 'bold'}}>Weight</span>
                    <br/>
                    {pokemon.weight}
                </Grid>
                <Grid item md={1}>
                    <span style={{fontWeight: 'bold'}}>Height</span>
                    <br/>
                    {pokemon.height}
                </Grid>
            </Grid>
            <br/>
            <Grid item xs={12} className={classes.pokemonInfo}>
                <span style={{fontWeight: 'bold', fontSize:'30px'}}>Stats</span>
                <Grid container justifyContent="center">
                {pokemon.stats.map((stats, index) => (    
                    <Grid item md={2} key={index}>
                    <span style={{fontWeight: 'bold'}}>{stats.stat.name}</span><br/>
                    {stats.base_stat} 
                    </Grid>
                ))}
                </Grid>
            </Grid>
            <br/>
            <Grid item xs={12} className={classes.pokemonInfo}>
                <span style={{fontWeight: 'bold'}}>Abilities</span>
                {pokemon.abilities.map((ability, index) => (
                    <Grid container justifyContent="center">
                        <Grid item md={2} key={index}>
                        {ability.ability.name}
                        </Grid>
                    </Grid>
                ))}
            </Grid>
            <br/>
            <Grid item xs={12} className={classes.pokemonInfo}>
                <span style={{fontWeight: 'bold', fontSize:'30px'}}>Moves</span>
                <Grid container justifyContent="center">
                {pokemon.moves.map((moves, index) => (    
                    <Grid item md={2} key={index}>
                    {moves.move.name}
                    </Grid>
                ))}
                </Grid>
            </Grid>
          </Grid>
        </Grid>
      ) : (
        <CircularProgress style={{ marginTop: 100 }} />
      )}
    </Box>
  );
};

export default PokemonDetails;
