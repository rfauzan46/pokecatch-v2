import axios from 'axios';
import React, { useEffect, useState } from 'react';
import PokemonCard from '../components/PokemonCard';
import { IMAGE_API_URL, POKEMON_API_URL } from '../config';
import { Box, CircularProgress, Grid, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  pokedexContainer: {
    textAlign: 'center',
    padding: '70px 10px 70px 10px',
    backgroundColor: '#fffecc'
  },
  buttons: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: 10
  },
  pageIndicator: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
  title: {
    fontSize: '50px',
    fontFamily: 'Roboto',
  }
}));

const Pokedex = () => {
  const classes = useStyles();
  const [currentPage, setCurrentPage] = useState(1);
  const [pokemonData, setPokemonData] = useState([]);

  useEffect(() => {
    axios.get(POKEMON_API_URL + "?limit=1000").then((response) => {
      if (response.status >= 200 && response.status < 300) {
        const { results } = response.data;
        let newPokemonData = [];
        results.forEach((pokemon, index) => {
          index++;
          let pokemonObject = {
            id: index,
            url: IMAGE_API_URL + index + ".png",
            name: pokemon.name,
          };
          newPokemonData.push(pokemonObject);
        });
        setPokemonData(newPokemonData);
      }
    });
  }, []);

  const handlePreviousPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleNextPage = () => {
    const totalPages = Math.ceil(pokemonData.length / 18);
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
  };

  const startIndex = (currentPage - 1) * 18;
  const endIndex = startIndex + 18;
  const displayedPokemons = pokemonData.slice(startIndex, endIndex);

  const totalPages = Math.ceil(pokemonData.length / 18);

  return (
    <Box>
      {pokemonData ? (
        <Grid className={classes.pokedexContainer} container spacing={2}>
          <Grid item xs={12}>
            <h1 className={classes.title}>Pokedex</h1>
          </Grid>
          {displayedPokemons.map((pokemon) => (
            <PokemonCard
              pokemon={pokemon}
              image={pokemon.url}
              key={pokemon.id}
            />
          ))}
          <div className={classes.buttons}>
            <button onClick={handlePreviousPage}>Previous</button>
            <button onClick={handleNextPage}>Next</button>
          </div>
          <div className={classes.pageIndicator}>
            Page {currentPage} of {totalPages}
          </div>
        </Grid>
      ) : (
        <CircularProgress style={{ marginTop: 100 }} />
      )}
    </Box>
  );
};

export default Pokedex;
