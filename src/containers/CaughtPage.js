import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PokemonCard from '../components/PokemonCard';
import { IMAGE_API_URL } from '../config';
import { Box, CircularProgress, Grid, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  caughtContainer: {
    textAlign: 'center',
    padding: '70px 10px 70px 10px',
    backgroundColor: '#fffecc',
  },
  title: {
    fontSize: '50px',
    fontFamily: 'Roboto',
  }
}));

const CaughtPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const caughtList = useSelector((state) => state.caughtList);

  return (
    <Box>
      {caughtList.length ? (
        <Grid className={classes.caughtContainer} container spacing={2}>
          <Grid item xs={12}>
            <h1 className={classes.title}>Caught Pokemons</h1>
          </Grid>
          {caughtList.map((pokemon) => (
            <PokemonCard
              key={pokemon.id}
              image={IMAGE_API_URL + pokemon.id + '.png'}
              pokemon={pokemon}
            />
          ))}
        </Grid>
      ) : (
        <CircularProgress style={{ marginTop: 100 }} />
      )}
    </Box>
  );
};

export default CaughtPage;
