import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'; // Import Redux Thunk
import caughtReducers from './reducers/caughtReducers';
import App from './App';
import { initializeCaughtList } from './actions/pokemonActions';
import { useDispatch } from 'react-redux';

const store = createStore(caughtReducers, applyMiddleware(thunk)); // Apply Redux Thunk middleware

const RootComponent = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const storedCaughtList = localStorage.getItem('caughtList');
    if (storedCaughtList) {
      const parsedCaughtList = JSON.parse(storedCaughtList);
      dispatch(initializeCaughtList(parsedCaughtList));
    }
  }, [dispatch]);

  return <App />;
};

ReactDOM.render(
  <Provider store={store}>
    <RootComponent />
  </Provider>,
  document.getElementById('root')
);
